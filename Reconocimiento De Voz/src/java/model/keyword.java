
package model;

/**
 * @author DarKPhuRioN
 */
public class keyword {
    private int Id;
    private String Word;
    
    public keyword()
    {
    
    }
    
    public keyword(int Id, String Word)
    {
        this.Id = Id;
        this.Word = Word;
    }
    
    public int GetId()
    {
        return Id;
    }
    
    public String GetWord()
    {
        return Word;
    }
}
