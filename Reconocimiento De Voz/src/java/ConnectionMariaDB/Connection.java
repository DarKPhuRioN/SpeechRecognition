package ConnectionMariaDB;
      
/**
 * @author DarKPhuRioN
 */

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public final class Connection {

    private static java.sql.Connection con;//instancia
    private static String drive = "jdbc:mysql";
    private String server = "127.0.0.1";
    private String port = "3306";
    private String db = "Speech_Recognition";
    private String user = "root";
    private String password = "phurion123";

    public Connection() 
    {
        openConnection();
    }

    public Connection(String server, String port, String db, String user, String password) 
    {
        this.server = server;
        this.port = port;
        this.db = db;
        this.user = user;
        this.password = password;
        openConnection();
    }

    private void openConnection() 
    {
        try 
        {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(drive + "://" + server + ":" + port + "/" + db, user, password);
        } 
        catch (SQLException | ClassNotFoundException ex) 
        {
            System.out.println(ex.toString());
        }
    }

    public java.sql.Connection getcon() 
    {
        return this.con;
    }

    public void closeConnection() throws SQLException 
    {
        con.close();
    }

    public boolean Operation(String req) throws SQLException 
    {
        Statement st = getcon().createStatement();
        try 
        {
            st.execute(req);
            return true;
        } 
            catch (SQLException e) 
        {
            System.out.println(e.toString());
            return false;
        }
    }

    public ArrayList Query(String request, String parameters[]) throws SQLException 
    {
        Statement st = con.createStatement();
        String arraytemp[];
        ArrayList<String[]> response = new ArrayList<>(); 
        try
        {
            ResultSet consulta = st.executeQuery(request);
            while (consulta.next()) 
            {
                arraytemp = new String[parameters.length];
                for (int i = 0; i < arraytemp.length; i++) {
                    arraytemp[i] = consulta.getString(parameters[i]);
                }
                response.add(arraytemp);
            }
            return response;
        }
            catch(SQLException e)
        {
            return response;
        }   
    }
 
//    public static void main(String[] args) throws SQLException {
//        ArrayList<String[]> cccc = new ArrayList<>();
//        Connection tt = new Connection();
//        String array[] = {"Id", "Word"};
//       cccc = tt.Query("select * from keywords", array);
//        System.out.println(cccc.size());
//       /* 
//        for (int i = 0; i < cccc.size(); i++) {
//            String x[] = (String[]) cccc.get(i);
//            System.out.println(x[0]+"-"+x[1]);
//        }*/
//        
//    }

}
