package Controllers;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import Service.Service;
import java.util.ArrayList;

/**
 * @author DarKPhuRioN
 */
@WebServlet(name = "controller", urlPatterns = {"/controller"})
public class controller extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
        
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Service ser = new Service();
        ArrayList todos;
        try {
            todos = ser.getAll();
            request.setAttribute("test", todos);
            String res = ser.getWords(request.getParameter("Word"));
            request.setAttribute("Word", res);
            request.getRequestDispatcher("mostrar.jsp").forward(request, response);
            response.sendRedirect("mostrar.jsp");
        } catch (Exception ex) {
            String res = "NO existe esa palabra en la Base de Datos   :  " + request.getParameter("Word");
            try {
                request.setAttribute("Word", res);
                request.getRequestDispatcher("mostrar.jsp").forward(request, response);
                response.sendRedirect("mostrar.jsp");
            } catch (Exception e) {
            }
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}