package Service;

import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author DarKPhuRioN
 */
public class Service 
{

    ConnectionMariaDB.Connection con = new ConnectionMariaDB.Connection();

    public int GetId(String Word) throws SQLException 
    {
        ArrayList<String[]> cccc = new ArrayList<>();
        String array[] = {"id"};
        cccc = con.Query("CALL Get('"+Word+"',1);", array);
        return Integer.parseInt(cccc.get(0)[0]);
    }
    
    public String getWords(String Word) throws SQLException
    {
        ArrayList<String[]> cccc = new ArrayList<>();
        String array[] = {"id","Word"};
        cccc = con.Query("CALL Get('"+Word+"',2);", array);
        return cccc.get(0)[0] +" - "+ cccc.get(0)[1];     
    }
    
    public ArrayList getAll() throws SQLException
    {
      
        String array[] = {"id","Word"};
        return  con.Query("SELECT * FROM keywords", array);
       
    }
}