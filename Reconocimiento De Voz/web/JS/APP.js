
var recognition, 
    recognizing = false,
    output = document.getElementById('output'),
    Word = document.getElementById('Word');
    
if (!('webkitSpeechRecognition' in window)) {
    alert("¡API no soportada!");
} else {
    recognition = new webkitSpeechRecognition();
    recognition.lang = "es-CO";
    recognition.continuous = true;
    recognition.interimResults = false;

    recognition.onstart = function () {
        recognizing = true;
        output.textContent = "Escuchando ...";
        console.log("empezando a eschucar");
    };

    recognition.onerror = function () {
        alert("Probablemente usted no dijo nada, o  su microfono no sirve");
    };
    
    recognition.onend = function() {
	recognizing = false;
	output.textContent = "Termino de escuchar";
	console.log("terminó de eschucar, llegó a su fin");
    };
    
    recognition.onresult = function (event) {
        //output.textContent = results[0][0].transcript;
        Word.value = event.results[0][0].transcript;
        
        sendForm();
    };
    
}

function sendForm()
{
    document.FS.submit();   
}