<%-- 
    Document   : mostrar
    Created on : 7/06/2018, 06:36:57 PM
    Author     : DarKPhuRioN
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="Es">
    
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="CSS/Style.css"/>
        <title>Resultado</title>
    </head>
    <body>
        <h1>Resultados / <a href="index.html">Regresar</a></h1>
        
        <h2 style="margin-left: 150px;">Su respuesta : <b>${Word}</b></h2>
        <table>
            <tr>
                <th>ID</th> 
                <th>Palabra</th> 
            </tr>
            
            <c:forEach items="${test}" var="it">
                <tr>
                    <td>${it[0]}</td>
                    <td>${it[1]}</td>
                </tr>
            </c:forEach>
        </table>


    </body>
</html>
