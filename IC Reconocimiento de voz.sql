DROP DATABASE IF EXISTS `Speech_Recognition`;

CREATE DATABASE `Speech_Recognition`;

USE `Speech_Recognition`;


CREATE TABLE `Keywords`
(
	`Id` INT NOT NULL PRIMARY KEY,
	`Word` VARCHAR( 60 ) NOT NULL
);


DELIMITER $$
#Option = 1 solo id, #Option = 2 id and word
CREATE PROCEDURE `Get`
(
	IN `Word` VARCHAR ( 60 ),
	IN `Option` INT
)
BEGIN
	IF( LENGTH( `Word` ) BETWEEN 3 AND 60 )
	AND( `Option`  BETWEEN  1 AND 2)
	THEN
		CASE 
			WHEN `Option` = 1 THEN

				SELECT
					k.`Id`
				FROM
					`Keywords` AS k
				WHERE
					k.`Word` = `Word`;
			WHEN `Option` = 2 THEN
				SELECT
					k.`Id`,
					k.`Word`
				FROM
					`Keywords` AS k
				WHERE
					k.`Word` = `Word`;
		END CASE;
	ELSE
		SIGNAL SQLSTATE 
        	'45000' 
    	SET 
        	MESSAGE_TEXT = _utf8 "Logintud minima incorrecta o Option invalida";
    END IF;
END;

$$
DELIMITER ;


DELIMITER $$

CREATE PROCEDURE `Insert_Word`
(
	IN `Word` VARCHAR ( 60 )
)
BEGIN
	DECLARE `ID` INT DEFAULT (SELECT COUNT(`Id`)+1 FROM `Keywords`);
	IF( (SELECT 
			K.`Id` 
		FROM 
			`Keywords` AS K 
		WHERE K.`Word` = `Word` ) IS NULL)
	AND ( LENGTH ( `Word` ) BETWEEN 3 AND 60 )
	THEN
		INSERT INTO 
			`Keywords`
		VALUES
			(
				ID,
				`Word`
			);
	ELSE
		SIGNAL SQLSTATE 
        	'45000' 
    	SET 
        	MESSAGE_TEXT = _utf8 "Logintud minima incorrecta o ya existe el registro"; 
	END IF;
END;

$$ 
DELIMITER ;


BEGIN;

	CALL `Insert_Word`('Sol');
	CALL `Insert_Word`('Maria');
	CALL `Insert_Word`('Amigo');
	CALL `Insert_Word`('Barco');
	CALL `Insert_Word`('Bote');
	CALL `Insert_Word`('Cuatro');
	CALL `Insert_Word`('Dia');
	CALL `Insert_Word`('Casa');
	CALL `Insert_Word`('Carro');
	CALL `Insert_Word`('Luna');
	CALL `Insert_Word`('Gato');
	CALL `Insert_Word`('Perro');
	CALL `Insert_Word`('Manzana');
	CALL `Insert_Word`('Computador');
	CALL `Insert_Word`('Rosa');
	CALL `Insert_Word`('Ciudad');
	CALL `Insert_Word`('Dibujo');
	CALL `Insert_Word`('Mesa');
	CALL `Insert_Word`('Negro');
	CALL `Insert_Word`('Blanco');
	CALL `Insert_Word`('Pantalones');
	CALL `Insert_Word`('Seno');
	CALL `Insert_Word`('Vaca');
	CALL `Insert_Word`('Planeta');
	CALL `Insert_Word`('Tierra');
	CALL `Insert_Word`('Rata');
	CALL `Insert_Word`('Burro');
	CALL `Insert_Word`('Torre');
	CALL `Insert_Word`('Silla');
	CALL `Insert_Word`('Memoria');
	CALL `Insert_Word`('Teclado');
	CALL `Insert_Word`('Lissa');
	CALL `Insert_Word`('Documentacion');
	CALL `Insert_Word`('Objetivo');
	CALL `Insert_Word`('Numero');
	CALL `Insert_Word`('Lunes');
	CALL `Insert_Word`('Diciembre');
	CALL `Insert_Word`('Mes');
	CALL `Insert_Word`('Ventana');
	CALL `Insert_Word`('Condon');

COMMIT;

#Obtener ID
CALL `Get`('Sol',1);

#Obtener ID and Word
CALL `Get`('Sol',2);
